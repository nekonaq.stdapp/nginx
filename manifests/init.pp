class nginx(
  Variant[String, Boolean] $ensure = present,
  Variant[String, Boolean] $service_ensure = running,
  Optional[Hash] $settings = {},
) inherits nginx::params {
  notice('---')

  $app_settings = $nginx::params::settings + std::nv($settings, Hash, {})
  info(std::pp({'app_settings' => $app_settings}))

  $file_ensure = std::file_ensure($ensure)
  $dir_ensure = std::dir_ensure($ensure)

  contain nginx::install
  contain nginx::config
  contain nginx::service

  case $ensure {
    'absent', false: {
      # absent の場合は disable service を先に行う
      Class[nginx::service]
      -> Class[nginx::install]
      -> Class[nginx::config]
    }
    default: {
      Class[nginx::install]
      -> Class[nginx::config]
      -> Class[nginx::service]
    }
  }
}

class nginx::install {
  #// config files
  file { 'nginx::config':
    path => "${nginx::app_dir}/config",
    ensure => $nginx::dir_ensure,
    force => true,
    require => File[nginx::app_dir],
  }

  file { 'nginx::config::env':
    path => "${nginx::app_dir}/config/nginx.env",
    ensure => $nginx::file_ensure,
    content => epp('nginx/config/nginx.env', $nginx::app_settings),
    require => File[nginx::config],
  }

  file { 'nginx::config::templates':
    path => "${nginx::app_dir}/config/nginx-templates",
    ensure => $nginx::dir_ensure,
    force => true,
    require => File[nginx::config],
  }

  file { 'nginx::config::templates::default':
    path => "${nginx::app_dir}/config/nginx-templates/default.conf.template",
    ensure => $nginx::file_ensure,
    content => epp('nginx/config/default.conf.template', $nginx::app_settings),
    require => File[nginx::config::templates],
  }

  #// setup service
  stdapp::service::install { nginx: }

  #// supplemental stuff
  file { 'nginx::html':
    path => "${nginx::app_dir}/html",
    ensure => $nginx::dir_ensure,
    force => true,
    recurse => true,
    require => File[nginx::app_dir],
    * => $nginx::dir_ensure ? {
      'directory' => {source => 'puppet:///modules/nginx/html'},
      default => {},
    },
  }
}

class nginx::config {
  #//empty
}

class nginx::service {
  stdapp::service { nginx: }
}
