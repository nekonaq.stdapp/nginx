class nginx::params {
  include stdapp

  $app_dir = "${stdapp::base_dir}/nginx"
  $service_prefix = 'docker-compose@nginx'

  $value = parseyaml(file('nginx/params.yml'))
  $settings = std::nv($value['nginx::settings'], Hash, {})
}
